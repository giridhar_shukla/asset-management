import { Response } from '@angular/http';
// import { TodoService } from './services/todo.service';
// import ToDo from './models/todo.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    // private todoService: TodoService
  ) { }

  // public newTodo: ToDo = new ToDo()

  // todosList: ToDo[];
  // editTodos: ToDo[] = [];

  assets = [
    {
      asset_id: 'MSCH546SS1',
      asset_name: 'laptop',
      status: 'Available'
    },
    {
      asset_id: 'MSCH546SS2',
      asset_name: 'mouse',
      status: 'Available'
    },
    {
      asset_id: 'MSCH546SS3',
      asset_name: 'laptop',
      status: 'Available'
    },
    {
      asset_id: 'MSCH546SS4',
      asset_name: 'laptop',
      status: 'Available'
    },
    {
      asset_id: 'MSCH546SS5',
      asset_name: 'laptop',
      status: 'Available'
    },
    {
      asset_id: 'MSCH546SS6',
      asset_name: 'wifi',
      status: 'Available'
    },
    {
      asset_id: 'MSCH546SS7',
      asset_name: 'mouse',
      status: 'Available'
    },
    {
      asset_id: 'MSCH546SS8',
      asset_name: 'JS definitive guide book',
      status: 'Issued'
    }
  ];


  settings = {
    columns: {
      asset_id: {
        title: 'Asset ID'
      },
      asset_name: {
        title: 'Asset',
        filter: {
          type: 'completer',
          config: {
            completer: {
              data: this.assets,
              searchFields: 'asset',
              titleField: 'asset',
            },
          },
        }
      },
      status: {
        title: 'Status',
        filter: {
          type: 'list',
          config: {
            selectText: 'All',
            list: [
              { value: 'Available', title: 'Available' },
              { value: 'Issued', title: 'Issued' }
            ],
          },
        }
      },
      issue_date: {
        title: 'Issue Date'
      },
      return_date: {
        title: 'Return Date'
      },
      issuer_id: {
        title: 'TSYS Id'
      },
      issuer_name: {
        title: 'Issuer'
      }
    }
  };

  data = [
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Available",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    },
    {
      asset_id: "MSCH546SS",
      asset_name: "laptop",
      status: "Issued",
      issue_date: "11/01/2017",
      return_date: "11/03/2017",
      issuer_id: "TSYS106719",
      issuer_name: 'Giridhar Shukla'
    }
  ];

  ngOnInit(): void {
    // this.todoService.getToDos()
    //   .subscribe(todos => {
    //     this.todosList = todos
    //     console.log(todos)
    //   })
  }


  create() {
    // this.todoService.createTodo(this.newTodo)
    //   .subscribe((res) => {
    //     this.todosList.push(res.data)
    //     this.newTodo = new ToDo()
    //   })
  }

editTodo(/*todo: ToDo*/) {
    // console.log(todo)
    // if(this.todosList.includes(todo)){
    //   if(!this.editTodos.includes(todo)){
    //     this.editTodos.push(todo)
    //   }else{
    //     this.editTodos.splice(this.editTodos.indexOf(todo), 1)
    //     this.todoService.editTodo(todo).subscribe(res => {
    //       console.log('Update Succesful')
    //     }, err => {
    //       this.editTodo(todo)
    //       console.error('Update Unsuccesful')
    //     })
    //   }
    // }
  }

  doneTodo(/*todo:ToDo*/){
    // todo.status = 'Done'
    // this.todoService.editTodo(todo).subscribe(res => {
    //   console.log('Update Succesful')
    // }, err => {
    //   this.editTodo(todo)
    //   console.error('Update Unsuccesful')
    // })
  }

submitTodo(/*event, todo:ToDo*/){
    // if(event.keyCode ==13){
    //   this.editTodo(todo)
    // }
  }

  deleteTodo(/*todo: ToDo*/) {
    // this.todoService.deleteTodo(todo._id).subscribe(res => {
    //   this.todosList.splice(this.todosList.indexOf(todo), 1);
    // })
  }


  title = 'app';


}
